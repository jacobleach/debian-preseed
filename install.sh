#!/bin/bash

set -e

name=$1
hostname=$2
domain=$3

echo "Root password"
root_pass_crypt=$(mkpasswd -m sha-512 -S "$(pwgen -ns 16 1)")
root_pass_crypt1=$(echo $root_pass_crypt | sed -e 's/[\/&]/\\&/g')

echo "User password"
user_pass_crypt=$(mkpasswd -m sha-512 -S "$(pwgen -ns 16 1)")
user_pass_crypt1=$(echo $user_pass_crypt | sed -e 's/[\/&]/\\&/g')

cp preseed.cfg.template preseed.cfg

sed -i "s/<%HOST-NAME%>/$hostname/" preseed.cfg
sed -i "s/<%DOMAIN%>/$domain/" preseed.cfg
sed -i "s/<%CRYPTED_ROOT_PASSWORD%>/"$root_pass_crypt1"/" preseed.cfg
sed -i "s/<%CRYPTED_USER_PASSWORD%>/"$user_pass_crypt1"/" preseed.cfg

virt-install \
  --connect=qemu:///system \
  --name=${name} \
  --ram=2048 \
  --vcpus=2 \
  --location http://ftp.us.debian.org/debian/dists/stable/main/installer-amd64/ \
  --disk size=10,pool=kvm-storage \
  --initrd-inject=preseed.cfg \
  --extra-args="auto=true"
